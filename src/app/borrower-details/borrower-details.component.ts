import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'borrower-details',
  templateUrl: './borrower-details.component.html',
  styleUrls: ['./borrower-details.component.css']
})
export class BorrowerDetailsComponent implements OnInit {

  @Input('borrowerGroup') public borrowerDetails: FormGroup;

  constructor() { }

  ngOnInit() { }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BorrowerDetailsComponent } from './borrower-details/borrower-details.component';
import { FinancialDetailsComponent } from './financial-details/financial-details.component';
import { BorrowerContactDetailsComponent } from './borrower-contact-details/borrower-contact-details.component';
import { BeneficiaryDetailsComponent } from './beneficiary-details/beneficiary-details.component';
import { SettlementInstructionsComponent } from './settlement-instructions/settlement-instructions.component';
import { FinancialSurveillanceComponent } from './financial-surveillance/financial-surveillance.component';
import { ImportPaymentsComponent } from './import-payments/import-payments.component';
import { FormHeaderComponent } from './form-header/form-header.component';

@NgModule({
  declarations: [ AppComponent, BorrowerDetailsComponent, FinancialDetailsComponent, BorrowerContactDetailsComponent, BeneficiaryDetailsComponent, SettlementInstructionsComponent, FinancialSurveillanceComponent, ImportPaymentsComponent, FormHeaderComponent ],
  imports: [ BrowserModule, FormsModule, ReactiveFormsModule, HttpModule ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'financial-surveillance',
  templateUrl: './financial-surveillance.component.html',
  styleUrls: ['./financial-surveillance.component.css']
})
export class FinancialSurveillanceComponent implements OnInit {

  @Input('financialSurveillanceGroup') public financialSurveillanceForm: FormGroup;

  public thirdPartyIndividuals = [
        { value: 'YES', display: 'YES' },
        { value: 'NO', display: 'NO' }
  ];
  
   public thirdPartyEntitys = [
        { value: 'YES', display: 'YES' },
        { value: 'NO', display: 'NO' }
    ];


  constructor() { }

  ngOnInit() {
  }

}

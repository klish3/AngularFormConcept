import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'borrower-contact-details',
  templateUrl: './borrower-contact-details.component.html',
})
export class BorrowerContactDetailsComponent implements OnInit {

  @Input('borrowerContactGroup') public borrowerConctactDetailsForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}

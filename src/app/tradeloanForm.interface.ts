export interface TradeloanForm {
    name: string;
    formHeader: FormHeader[];
    borrowerDetails: BorrowerDetails[];
    borrowerContactDetails:BorrowerContactDetails[];
    financialDetails: FinancialDetails[];
    beneficiaryDetails: BeneficiaryDetails[];
    settlementInstructions: SettlementInstructions[];
    financialSurveillance: FinancialSurveillance[];

    natureOfPayment:string;
    thirdPartyEntity: string;
    relationshipBetweenParties:string;

    importPayments: ImportPayments[];
}

export interface FormHeader {

    tradeLoanSelection: string;
    applicationDate: string;
    financialOption: string;

}

export class BorrowerDetails {

    borrowerName: string;
    borrowerCompanyRegistration: string;
    borrowerStreetAddress: string;
    borrowerSuburb: string;
    borrowerCity: string;
    borrowerPostalCode: string;

}

export interface BorrowerContactDetails {

    borrowerPrimaryContactPersonFullName: string;
    borrowerTelephoneWork: string;
    borrowerTelephoneCell: string;
    borrowerFax: string;
    borrowerEmail: string;

}

export interface FinancialDetails{

    financeDiscountRequirefor: string;
    proposedDrawdownDate: string;
    maturityDate: string;
    totalAmount:string;
    totalAmountCurrency:string;
    loanAmountRequestInDomestic: string;
    loanAmountRequestInInternational:string;

}
export interface BeneficiaryDetails{

    beneficiaryRSAResident: boolean;
    beneficiaryName: string;
    beneficiarySurname: string;
    beneficiaryStreetAddress: string;
    beneficiarySuburb: string;
    beneficiaryCity: string;
    beneficiaryPostalCode: string;
    beneficiaryProvince: string;
    beneficiaryCountry: string;
    beneficiaryBank: string;
    beneficiaryIBAN: string;
    beneficiarySwiftCode: string;
    beneficiaryGoodsCountryOfOrigin: string;
    beneficiaryPrimaryContactPersonFullName: string;
    beneficiaryAccountNUMDebited: string;
    beneficiaryChargeDetails: string;

}
export interface SettlementInstructions{

    accountName:string;
    accountNumber:string;
    accountType:string;
    currencyConversion:string;
    settlementContactNum:string;
    settlementRate:string;
    settlementBank:string;
    settlementBranch:string;
    otherPaymentMethod:string;
    natureOfPayment:string;

}
export interface FinancialSurveillance{

  thirdPartyIndividual: string;
  natureOfPayment:string;
  thirdPartyEntity: string;
  relationshipBetweenParties:string;
  mrn:string;
  tdn:string;
  ccn:string;

}
export interface ImportPayments{

  mrn:string;
  tdn:string;
  ccn:string;

}
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'beneficiary-details',
  templateUrl: './beneficiary-details.component.html',
  styleUrls: ['./beneficiary-details.component.css']
})
export class BeneficiaryDetailsComponent implements OnInit {

  @Input('beneficiaryDetailsGroup') public beneficiaryDetailsForm: FormGroup;

  public beneficiaryRSAResidents = [
        { value: 'YES', display: 'YES' },
        { value: 'NO', display: 'NO' }
  ];

   public beneficiaryChargeDetails = [
        { value: 'SHA: charges are shared', display: 'SHA: charges are shared' },
        { value: 'BEN: All charges are paid for by beneficiary', display: 'BEN: All charges are paid for by beneficiary' },
        { value: 'OUR: All charges including overseas charges are paid for by borrower', display: 'OUR: All charges including overseas charges are paid for by borrower' }
    ];

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'financial-details',
  templateUrl: './financial-details.component.html',
  styleUrls: ['./financial-details.component.css']
})


export class FinancialDetailsComponent implements OnInit {

  @Input('financialDetailsGroup') public financialDetailsForm: FormGroup;

  public loanAmountRequestInInternationals = [
        { value: 'ZAR', display: 'ZAR' },
        { value: 'EUR', display: 'EUR' },
        { value: 'USD', display: 'USD' },
        { value: 'GBP', display: 'GBP' },
        { value: 'JPY', display: 'JPY' }
    ];

  constructor() { }

  

  ngOnInit() {  
    
  }

}

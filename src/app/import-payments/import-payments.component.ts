import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'import-payments',
  templateUrl: './import-payments.component.html',
  styleUrls: ['./import-payments.component.css']
})
export class ImportPaymentsComponent implements OnInit {

  @Input('importantPaymentsGroup') public importantPaymentsForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}

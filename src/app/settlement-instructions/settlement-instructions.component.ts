import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'settlement-instructions',
  templateUrl: './settlement-instructions.component.html',
  styleUrls: ['./settlement-instructions.component.css']
})
export class SettlementInstructionsComponent implements OnInit {

  @Input('settlementInstructionsGroup') public settlementInstructionsForm: FormGroup;

  public accountTypes = [
        { value: 'Cheque', display: 'Cheque' },
        { value: 'Post', display: 'Foreign Currency account' }
  ];
  
  public currencyConversions = [
        { value: 'Convert at Spot', display: 'Convert at Spot' },
        { value: 'Drawdown against FEC contract', display: 'Drawdown against FEC contract' }
  ];
  
  constructor() { }

  ngOnInit() {
  }

}

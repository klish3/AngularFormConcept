import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { BorrowerDetails } from './tradeloanForm.interface'

declare var pdfMake: any;
declare var buildPdf: any;

@Component({
  
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/css/app.component.css','../assets/css/animate.css']

})
export class AppComponent implements OnInit {
   title = 'app works!';

   public form: FormGroup;
   public submitted: boolean;
   pdf: any;
    
    constructor(private _fb: FormBuilder) { }

   
    ngOnInit() {
        this.form = this._fb.group({
            formHeader: this._fb.array([
                this.initFormHeader(),
            ]),
            borrowerDetails: this._fb.array([
                this.initBorrowerDetials(),
            ]),
            borrowerContactDetails: this._fb.array([
                this.initBorrowerContactDetials(),
            ]),
            financialDetails: this._fb.array([
                this.initFinancialDetials(),
            ]),
            beneficiaryDetails: this._fb.array([
                this.initBeneficiaryDetials(),
            ]),
            settlementInstructions: this._fb.array([
                this.initSettlementInstructions(),
            ]),
            financialSurveillance: this._fb.array([
                this.initFinancialSurveillance(),
            ]),
            importPayments: this._fb.array([
                this.initImportPayments(),
            ]),
            test:false,
            isMessage:false,
            isSubmitMessage:false,
        });
    }

    initFormHeader() {
        return this._fb.group({
            tradeLoanSelection: '',
            applicationDate: '',
            financialOption: '',  
        });
    }
    initBorrowerDetials() {
        // return this._fb.group(new BorrowerDetails());
         return this._fb.group({
            borrowerName: '',
            borrowerCompanyRegistration: '',
            borrowerStreetAddress: '',
            borrowerSuburb: '',
            borrowerCity: '',
            borrowerPostalCode: '',
        });

    }
    initBorrowerContactDetials() {
        return this._fb.group({
            borrowerPrimaryContactPersonFullName:'',
            borrowerTelephoneWork:'',
            borrowerTelephoneCell:'',
            borrowerFax:'',
            borrowerEmail:'',
        });
    }
    initFinancialDetials() {
        return this._fb.group({
            financeDiscountRequirefor: '',
            proposedDrawdownDate: '',
            maturityDate: '',
            totalAmount:'',
            totalAmountCurrency:'',
            loanAmountRequestInDomestic: '',
            loanAmountRequestInInternational: '',
        });
    }
    initBeneficiaryDetials() {
        return this._fb.group({
            beneficiaryRSAResident: [''],
            beneficiaryName: [''],
            beneficiaryStreetAddress: [''],
            beneficiarySuburb: [''],
            beneficiaryCity: [''],
            beneficiaryPostalCode: [''],
            beneficiaryProvince: [''],
            beneficiaryCountry: [''],
            beneficiaryBank: [''],
            beneficiaryIBAN: [''],
            beneficiarySwiftCode: [''],
            beneficiaryGoodsCountryOfOrigin: [''],
            beneficiaryAccountNUMDebited: [''],
            beneficiaryChargeDetail: [''],
        });
    }
    initSettlementInstructions() {
        return this._fb.group({
            accountName:[''],
            accountNumber:[''],
            accountType:[''],
            currencyConversion:[''],
            settlementContactNum:[''],
            settlementRate:[''],
            settlementBank:[''],
            settlementBranch:[''],
            otherPaymentMethod:[''],
        });
    }
    initFinancialSurveillance() {
        return this._fb.group({
            thirdPartyIndividual: [''],
            natureOfPayment:[''],
            thirdPartyEntity: [''],
            relationshipBetweenParties:[''],
        });
    }
    initImportPayments() {
        return this._fb.group({
            mrn:[''],
            tdn:[''],
            ccn:[''], 
        });
    }

    save(form: FormGroup, isValid: boolean) {
          this.submitted = true;
          this.pdf = pdfMake;
          this.pdf.createPdf(buildPdf(form)).open();
    }
}

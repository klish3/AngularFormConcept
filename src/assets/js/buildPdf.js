function buildPdf(value) {
    console.log('value = '+value)
    var formContent = value;
    var docDefinition = {
      content: [ 

        { text: 'BorroerDetails '},

        { text: 'borrowerName: ' + formContent.borrowerDetails[0].borrowerName },
        { text: 'borrowerCompanyRegistration: ' + formContent.borrowerDetails[0].borrowerCompanyRegistration },
        { text: 'borrowerStreetAddress: ' + formContent.borrowerDetails[0].borrowerStreetAddress },
        { text: 'borrowerSuburb: ' + formContent.borrowerDetails[0].borrowerSuburb },
        { text: 'borrowerCity: ' + formContent.borrowerDetails[0].borrowerCity },
        { text: 'borrowerPostalCode: ' + formContent.borrowerDetails[0].borrowerPostalCode },
        { text: 'borrowerPrimaryContactPersonFullName: ' + formContent.borrowerDetails[0].borrowerPrimaryContactPersonFullName },
        { text: 'borrowerTelephoneWork: ' + formContent.borrowerDetails[0].borrowerTelephoneWork },
        { text: 'borrowerTelephoneCell: ' + formContent.borrowerDetails[0].borrowerTelephoneCell },
        { text: 'borrowerFax: ' + formContent.borrowerDetails[0].borrowerFax },
        { text: 'borrowerEmail: ' + formContent.borrowerDetails[0].borrowerEmail },

        { text: 'borrowerContactDetails '},

    { text: '' + formContent.borrowerContactDetails[0].borrowerPrimaryContactPersonFullName },
    { text: '' + formContent.borrowerContactDetails[0].borrowerTelephoneWork },
    { text: '' + formContent.borrowerContactDetails[0].borrowerTelephoneCell },
    { text: '' + formContent.borrowerContactDetails[0].borrowerFax },
    { text: '' + formContent.borrowerContactDetails[0].borrowerEmail },

    { text: 'financialDetails '},
 
    { text: '' + formContent.financialDetails[0].financeDiscountRequirefor },
    { text: '' + formContent.financialDetails[0].proposedDrawdownDate },
    { text: '' + formContent.financialDetails[0].maturityDate },
    { text: '' + formContent.financialDetails[0].totalAmount },
    { text: '' + formContent.financialDetails[0].totalAmountCurrency },
    { text: '' + formContent.financialDetails[0].loanAmountRequestInDomestic },
    { text: '' + formContent.financialDetails[0].loanAmountRequestInInternational },

    { text: 'beneficiaryDetails '},

    { text: '' + formContent.beneficiaryDetails[0].beneficiaryRSAResident },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryName },
    { text: '' + formContent.beneficiaryDetails[0].beneficiarySurname },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryStreetAddress },
    { text: '' + formContent.beneficiaryDetails[0].beneficiarySuburb },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryCity },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryPostalCode },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryProvince },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryCountry },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryBank },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryIBAN },
    { text: '' + formContent.beneficiaryDetails[0].beneficiarySwiftCode },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryGoodsCountryOfOrigin },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryPrimaryContactPersonFullName },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryAccountNUMDebited },
    { text: '' + formContent.beneficiaryDetails[0].beneficiaryChargeDetails },

    { text: 'settlementInstructions '},
    
    { text: '' + formContent.settlementInstructions[0].accountName },
    { text: '' + formContent.settlementInstructions[0].accountNumber },
    { text: '' + formContent.settlementInstructions[0].accountType },
    { text: '' + formContent.settlementInstructions[0].currencyConversion },
    { text: '' + formContent.settlementInstructions[0].settlementContactNum },
    { text: '' + formContent.settlementInstructions[0].settlementRate },
    { text: '' + formContent.settlementInstructions[0].settlementBank },
    { text: '' + formContent.settlementInstructions[0].settlementBranch },
    { text: '' + formContent.settlementInstructions[0].otherPaymentMethod },
    { text: '' + formContent.settlementInstructions[0].natureOfPayment },

    { text: 'financialSurveillance '},

    { text: '' + formContent.financialSurveillance[0].thirdPartyIndividual },

    { text: 'Normal Model '},

    { text: '' + formContent.natureOfPayment },
    { text: '' + formContent.thirdPartyEntity },
    { text: '' + formContent.relationshipBetweenParties },

    { text: 'importPayments '},

    { text: '' + formContent.importPayments[0].mrn },
    { text: '' + formContent.importPayments[0].tdn },
    { text: '' + formContent.importPayments[0].ccn },
          
        
        ]
    }
    
    return docDefinition;
}
    



    // formContent.borrowerContactDetails[0].borrowerPrimaryContactPersonFullName
    // formContent.borrowerContactDetails[0].borrowerTelephoneWork
    // formContent.borrowerContactDetails[0].borrowerTelephoneCell
    // formContent.borrowerContactDetails[0].borrowerFax
    // formContent.borrowerContactDetails[0].borrowerEmail

    
 

    // formContent.financialDetails[0].financeDiscountRequirefor
    // formContent.financialDetails[0].proposedDrawdownDate
    // formContent.financialDetails[0].maturityDate
    // formContent.financialDetails[0].totalAmount
    // formContent.financialDetails[0].totalAmountCurrency
    // formContent.financialDetails[0].loanAmountRequestInDomestic
    // formContent.financialDetails[0].loanAmountRequestInInternational

    // formContent.beneficiaryDetails[0].beneficiaryRSAResident
    // formContent.beneficiaryDetails[0].beneficiaryName
    // formContent.beneficiaryDetails[0].beneficiarySurname
    // formContent.beneficiaryDetails[0].beneficiaryStreetAddress
    // formContent.beneficiaryDetails[0].beneficiarySuburb
    // formContent.beneficiaryDetails[0].beneficiaryCity
    // formContent.beneficiaryDetails[0].beneficiaryPostalCode
    // formContent.beneficiaryDetails[0].beneficiaryProvince
    // formContent.beneficiaryDetails[0].beneficiaryCountry
    // formContent.beneficiaryDetails[0].beneficiaryBank
    // formContent.beneficiaryDetails[0].beneficiaryIBAN
    // formContent.beneficiaryDetails[0].beneficiarySwiftCode
    // formContent.beneficiaryDetails[0].beneficiaryGoodsCountryOfOrigin
    // formContent.beneficiaryDetails[0].beneficiaryPrimaryContactPersonFullName
    // formContent.beneficiaryDetails[0].beneficiaryAccountNUMDebited
    // formContent.beneficiaryDetails[0].beneficiaryChargeDetails
    
    // formContent.settlementInstructions[0].accountName
    // formContent.settlementInstructions[0].accountNumber
    // formContent.settlementInstructions[0].accountType
    // formContent.settlementInstructions[0].currencyConversion
    // formContent.settlementInstructions[0].settlementContactNum
    // formContent.settlementInstructions[0].settlementRate
    // formContent.settlementInstructions[0].settlementBank
    // formContent.settlementInstructions[0].settlementBranch
    // formContent.settlementInstructions[0].otherPaymentMethod
    // formContent.settlementInstructions[0].natureOfPayment

    // formContent.financialSurveillance[0].thirdPartyIndividual

    // formContent.natureOfPayment
    // formContent.thirdPartyEntity
    // formContent.relationshipBetweenParties

    // formContent.importPayments[0].mrn
    // formContent.importPayments[0].tdn
    // formContent.importPayments[0].ccn


import { TradePortalPage } from './app.po';

describe('trade-portal App', function() {
  let page: TradePortalPage;

  beforeEach(() => {
    page = new TradePortalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
